(ns ClojureProject.List)

; Questions from 99 problems to solve

(def x ["a","a","b","b","b","c","d","d"])
(defn myfreq [xs] "My Implementionn of frequencies"
  "Returns a map from distinct items in coll to the number of times
  they appear."
  (persistent!
   (reduce (fn [counts x]
             (assoc! counts x (inc (get counts x 0))))
           (transient {}) xs)))

(defn freq [xs]
     (let [pk (group-by identity x)]
       (zipmap (keys pk) (map #(count (second %)) pk))))

(defn isperfect? [num]
(
= num
(reduce + (into [1] (set (flatten ( for [x ( filter (fn [s](zero? (mod num s)))
(range 2 (inc (Math/floor(Math/sqrt num))) )) :let [y (quot num x)]] [x y])))))))



(defn mdistinct [xs]
  " Removes duplicates from sequence"
  (loop [x xs y [] z {}]
    (if (empty? x) y
      ( if (not(contains? z (first x)))
        (recur (rest x) ( conj y (first x)) (assoc z (first x) 1))
        (recur (rest x) y z)
        )
      )
    )
  )
(defmacro dbg[x] `(let [x# ~x] (println "dbg:" '~x "=" x#) x#))


	(defn isHappy? [num]
	  (loop [ x num  seen #{}]
	    
	      (if (= x 1) true
	      ( if (contains? seen x) false
	        (recur (reduce + (map (fn[x] (* x x ))(map (fn[x] (Character/getNumericValue x))(str x)))) (conj seen x))
	        )
	      )
	      )
	      )
;; using clojure higher order functions
;; right to left
(defn composite [x y]
( let [ x x y y ]
(fn [a]
(apply x (vector (apply y (vector a))))))
)

;; Clojure Function composition

(defn my-comp [& y]
        (let [x y]
          (fn [a]
            
              (loop [z (reverse x) ret a]
              (if ( empty? z) 
                ret
                  (recur (rest z)  (apply (first z) (vector ret)))
                  )))))   
;; frequencies implementatio
(defn frew [xs]
  (into {} ( for [x (distinct xs) :let [y (count (filter #(#{x} %1) xs))]] [x y]))
  )
;; lazy palindrome Prob 150
(defn lazy-palindrome [x] ( filter (fn [x] (= (seq (str x)) (reverse (str x)))) (iterate inc x)))

;; gcd
(defn gcd [a b]
        (loop [a a b b]
          (if (zero? b) a
            (recur b (mod a b)
                   ))))
;; Euler's totient function (4clojure 75 Medium)

(defn totient [x]
                             ( if(= 1 x) 1
                              ( count(filter #(= 1 ( (fn [a b] (loop [a a b b]
                                                                ( if(zero? b)a
                                                                  (recur b (mod a b)))
                                                                
                                                                ) ) %1 x)) (range 1 x)))))
;; Anagram finder MEdium Prob 77
(defn anagram [x](into #{} (map set (vals (filter (fn [x] (>= (count (second x)) 2)) (group-by #(apply str(sort %1)) x))))))

;; ultimate crisp solution
#(->> %
       (group-by sort)
       vals
       (filter (fn [s] (< 1 (count s))))
       (map set)
       set)

;; Problem 50 split By Type
#( vals (group-by type %1))

;; problem sorting words
#(sort-by clojure.string/lower-case (re-seq #"\w+" %))

;; Sequs Horribilis
 ;;Create a function which takes an integer and a nested collection of integers as arguments. Analyze the elements of the input collection 
;;and return a sequence which maintains the nested structure, and which includes all elements starting from the head whose sum is less than or equal to the input integer.
(defn sequs-horribilis [n xs]
 (letfn [(f [ys cnt lmt]
           (let [x (first ys) y (rest ys)]
             (cond
              (empty? ys) []
              (and (coll? x) (empty? y)) (list (f x cnt lmt))
              (coll? x) (list (f (lazy-seq (conj x y)) cnt lmt))
              (> (+ cnt x) lmt) []
              :else (lazy-seq (cons x (f y (+ cnt x) lmt))))))]
   (f xs 0 n)))
          
        
  ;; isprime?
(defn isprime? [n]
     (cond
       (= n 2) true
       (= n 3) true
       :else 
      (= (count ( filter #(zero? (mod n %)) (range 2 (inc (Math/floor(Math/sqrt n)))))) 0)
      )
     )
 ;;ptime-upto
 (defn  prime-upto [x] ( take x (filter (fn [n]
        (cond
          (= n 2) true
          (= n 3) true
          :else 
         (= (count ( filter #(zero? (mod n %)) (range 2 (inc (Math/floor(Math/sqrt n)))))) 0)
         )
        ) (iterate inc 2)))) 
 ;; intoCamelCase medium 102
 (defn intoCamelCase [x] ( let [y (re-seq #"\w+" x)]
                        (clojure.string/join (conj (map #(clojure.string/capitalize %1) (rest y)) (first y)))
                        )
                )
 ;; Powerset
 (defn powerset [x]  (conj (set (map set (set (reduce (fn [x y] (concat (map #(concat %1 #{y}) x ) x #{#{y}})) #{} x)))) #{}))
 ;; nice powerset
 (defn p [s]
  (if (empty? s) #{#{}}
      (let [t (p (rest s))]
        (into t
              (map #(conj % (first s)) t)))
      ))
;; balanced number
;;A balanced number is one whose component digits have the same sum on the left and right halves of the number.
;; Write a function which accepts an integer n, and returns true iff n is balanced.
(defn isbalanced? [x]
     (let [ y (vec (map (fn[x] (Character/getNumericValue x))(str x)))]
       (cond
         (= 1 (count y)) true
         (= 2 (count y)) (if (= (first y) (second y)) true false)
         (> 2 (count y)) ( if ( comp not zero? (mod (count y) 2)) (let [z (quot (count y) 2)] ( 
                                                                                              (= (reduce + (subvec y 0 z)) (reduce + (subvec y (inc z) (count y)))) 
                                                                                               )) false))))
;; reverse interleave Medium Porb 47
(defn reverseInterleave [xs x] (apply map vector (partition x xs)))

;; ;; flipping out: A higher-order function which flips the order of the arguments of an input function
(defn fliping-arguments[ f & args]
  ( fn [& a]
                      ( f (second a) (first a))
                      )
                    )
;; juxtposition Take a set of functions and return a 
;; new function that takes a variable number of arguments and returns a sequence containing the result of applying each function left-to-right to the argument list
(defn juxtaposition [& xs]
        ( fn [ & args]
          ( for [ x xs ] ( apply x args))))
;; merge with function

(defn mergeFunc [f & maps]
  (reduce
    (fn [a b]
      (reduce
        (fn [x [k v]]
          (assoc x k (if (b k) (f v (b k)) v)))
        b a))
    (first maps) (rest maps)))

;; myflatten (does not produce a lazy sequence
(defn  my-flatten [xs]
                    ( loop [ x xs y []]
                      ( if (empty? x) y
                        ( if (sequential? (first x)) (concat (my-flatten (first x)) (my-flatten (rest x)))
                          (recur (rest x) (conj y (first x))) ))))
;;
(defn pflatten [xs]
        (if (every? sequential? xs)
          (mapcat pflatten xs)
          [xs]))

;; problem 105 Identify key and values
(defn keys-and-values [xs]
        ( loop [ x xs y {}]
          ( if( empty? x) y
            (recur (drop-while #( (comp not keyword?) %) (rest x)) (assoc y (first x) (take-while #( (comp not keyword?) %) (rest x))) )
            )
          )
        )
;; problem 105
(defn testreify [& x]
                                   (reify Object clojure.lang.Seqable
                                     (toString [this] (clojure.string/join "," (sort x)) ) 
                                     (seq [this] (seq (distinct x)))
                                     ))
;; bases vey easy
( #(loop [a %1 b %2 z [] ]
  ( if (zero? (quot a b)) (reverse (conj z (rem a b)))
    (recur  (quot a b) b (conj z (rem a b)) )
 
    )) )
;; pronounciation

#(rest (iterate (fn [coll] (mapcat (juxt count first) (partition-by identity coll))) %))
  
;; oscilirate
(defn osclirate [x & fs]
     (letfn [(oscillate [x fs]
               (cons x (lazy-seq (oscillate ((first fs) x) (rest fs)))))]
       (oscillate x (cycle fs))))

;; k-combinations 
;Given a sequence S consisting of n elements generate all k-combinations of S, i. e. generate all possible sets consisting 
;;of k distinct elements taken from S. The number of k-combinations for a sequence is equal to the binomial coefficient.

(defn k-combinations [ x y]
           ( set (filter #(= x (count %)) (powerset y))))